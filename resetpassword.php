<!-- This is the script which handles the emailed pswd change links -->
<!-- ( NOT the script which generates the email link itself. -->
<!-- That script is located at /projex/resetpassword/index.php ) -->

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ProjectPro - Reset Password</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            	<!--    
            	<a class="navbar-brand" href="index.html">SB Admin</a>
            	-->
            	<a href="index.html"><img src="img/logo-web-banner.png" style="display: block; margin: 0 auto;"></a>
            </div>
            <!-- Top Menu Items (user only) -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - Adjust as per login status -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="#"><i class="fa fa-fw fa-dashboard"></i> Menu One</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-bar-chart-o"></i> Menu Two</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-table"></i> Menu Three</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-edit"></i> Menu Four</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-desktop"></i> Menu Five</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-wrench"></i> Menu Six</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">
            
<?PHP

$em = $_GET["emailaddr"];
$rqkey = $_GET["token"];
$sq = "'";
$comma = ",";

?>

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Reset Password
                            <small>for user <?php echo $em;?></small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Reset Password
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                
                    <div class="row">
                    <div class="col-lg-3 text-center">
                        <!-- Nothing goes here -->
                    </div>
                    <div class="col-lg-6 text-left">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                
                                
<?PHP

                                
// Retrieve the POST array values
                                
// Query the PW_CH_RQ table (by both email addr and Key value), retrieve exp. datetime
                                
include 'include/db.conf.php';
                                
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
	
// Check connection
if ($conn->connect_error) {
// Database connection error, write to screen
	echo "Sorry, there has been a database connection error: ". $conn->connect_error;
} else {
	$sql = "SELECT expires, been_used FROM PW_CH_RQ WHERE email_addr = " . $sq . $em . $sq . " AND rq_key = " . $sq . $rqkey . $sq;
	if ($result = $conn->query($sql)) {   
		if ($result->num_rows === 1) {   // matching row found
			$resultArray = $result->fetch_assoc();
			$expDateStr  = $resultArray['expires'];
			$beenUsed    = $resultArray['been_used'];
			$now = new DateTime("now");
			$exp = new DateTime($expDateStr);
			if (($now > $exp) || ($beenUsed === 1)) {   // link has expired, write to screen
				// echo $beenUsed . "<br>";
				echo "Sorry, that Password Change link has either expired (older than 24 hours) or has already been used. Click the button below to try again.<br><br>\n
				<form method='post' action='sendpasswordchangeemail.php'>\n
				<input type='hidden' name='emailaddr' value='" . $em . "'>\n
				<input type='submit' class='btn btn-primary btn-default' value='Resend Password Change Link'>\n
				</form>";
			} else {
				
                                
echo "                                
<form role='form' name='login' id='passwordForm' method='post' action='pswdchange.php'>
	<div class='form-group'>
		<label>Please enter your new password:</label>
	</div>  <!-- /.form-group -->
	
	<input type='password' class='input-lg form-control' name='password1' id='password1' placeholder='New Password' autocomplete='off'>
	<div class='row'>
		<div class='col-sm-6'>
			<span id='8char' class='glyphicon glyphicon-remove' style='color:#FF0004;'></span> 8 Characters Long<br>
			<span id='ucase' class='glyphicon glyphicon-remove' style='color:#FF0004;'></span> One Uppercase Letter
		</div>
	<div class='col-sm-6'>
		<span id='lcase' class='glyphicon glyphicon-remove' style='color:#FF0004;'></span> One Lowercase Letter<br>
		<span id='num' class='glyphicon glyphicon-remove' style='color:#FF0004;'></span> One Number
	</div>
	</div>
		<input type='password' class='input-lg form-control' name='password2' id='password2' placeholder='Repeat Password' autocomplete='off'>
	<div class='row'>
		<div class='col-sm-12'>
			<span id='pwmatch' class='glyphicon glyphicon-remove' style='color:#FF0004;'></span> Passwords Match
		</div>
	</div>
	<br>
		<input type='hidden' name='emailaddr' value='" . $em . "'>\n
		<input type='submit' class='btn btn-primary btn-load btn-default' data-loading-text='Changing Password...' value='Change Password'>
</form>
";

			}
		} else {
			echo "Sorry, information corresponding with that password change link could not be found. Please contact <a href-'mailto:support@projectprohub.com'>support@projectprohub.com</a> for further assistance.";	
		}
	} else {	
	echo "Sorry, an unexpected SQL error has occured: " . $conn->error . "... Please contact <a href-'mailto:support@projectprohub.com'>support@projectprohub.com</a> for further assistance.";	
	}
}

?>
								
								
								
                            </div>  <!-- /.panel-body -->
                        </div>
                    </div>
                    <div class="col-lg-3 text-center">
                         <!-- Nothing goes here -->
                    </div>
                </div>
                <!-- /.row -->




            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    
    <!-- Password Validation Javascript -->
    
    <script type="text/javascript">
	$("input[type=password]").keyup(function(){
    var ucase = new RegExp("[A-Z]+");
	var lcase = new RegExp("[a-z]+");
	var num = new RegExp("[0-9]+");
	
	if($("#password1").val().length >= 8){
		$("#8char").removeClass("glyphicon-remove");
		$("#8char").addClass("glyphicon-ok");
		$("#8char").css("color","#00A41E");
	}else{
		$("#8char").removeClass("glyphicon-ok");
		$("#8char").addClass("glyphicon-remove");
		$("#8char").css("color","#FF0004");
	}
	
	if(ucase.test($("#password1").val())){
		$("#ucase").removeClass("glyphicon-remove");
		$("#ucase").addClass("glyphicon-ok");
		$("#ucase").css("color","#00A41E");
	}else{
		$("#ucase").removeClass("glyphicon-ok");
		$("#ucase").addClass("glyphicon-remove");
		$("#ucase").css("color","#FF0004");
	}
	
	if(lcase.test($("#password1").val())){
		$("#lcase").removeClass("glyphicon-remove");
		$("#lcase").addClass("glyphicon-ok");
		$("#lcase").css("color","#00A41E");
	}else{
		$("#lcase").removeClass("glyphicon-ok");
		$("#lcase").addClass("glyphicon-remove");
		$("#lcase").css("color","#FF0004");
	}
	
	if(num.test($("#password1").val())){
		$("#num").removeClass("glyphicon-remove");
		$("#num").addClass("glyphicon-ok");
		$("#num").css("color","#00A41E");
	}else{
		$("#num").removeClass("glyphicon-ok");
		$("#num").addClass("glyphicon-remove");
		$("#num").css("color","#FF0004");
	}
	
	if($("#password1").val() == $("#password2").val()){
		$("#pwmatch").removeClass("glyphicon-remove");
		$("#pwmatch").addClass("glyphicon-ok");
		$("#pwmatch").css("color","#00A41E");
	}else{
		$("#pwmatch").removeClass("glyphicon-ok");
		$("#pwmatch").addClass("glyphicon-remove");
		$("#pwmatch").css("color","#FF0004");
	}
});
    </script>


</body>

</html>