<?php

if (isset($_POST['firstname']))  {

	include 'include/db.conf.php';
	include 'admin-sendpasswordsetemail.php';

	$firstname = $_POST['firstname'];
	$lastname = $_POST['lastname'];
	$emailaddr = $_POST['emailaddr'];
	$usertype = $_POST['usertype'];
	
	$em = $_POST['emailaddr'];
	$pw = password_hash('temppswd', PASSWORD_DEFAULT);
	$fn = $_POST['firstname'];
	$ln = $_POST['lastname'];
	$ae = "unset";
	$et = $_POST['usertype'];
	$sq = "'";
	$fb = "', '";
	$valuesString = $sq . $em . $fb . $pw . $fb . $fn . $fb . $ln . $fb . $et . $fb . $ae . $sq;
	
	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	
	// Check connection
	if ($conn->connect_error) {
		$status = "Error";
		$statusMsg = "Connection failed: " . $conn->connect_error;
	} else {
	
	if ($result = $conn->query("SELECT first_name, last_name FROM USERS WHERE email_addr = '" . $em . "'")) {
		if ($result->num_rows > 0) {
			$status = "Error";
			$statusMsg = "email already exists";
		} else {

			$sql = "INSERT INTO USERS (email_addr, pw_hash, first_name, last_name, user_type, acct_email)
					VALUES (" . $valuesString . ")";

		  if ($conn->query($sql) === TRUE) {
 		     $status = "Success";
 		     $statusMsg = "New record created successfully";
      
 		     // Send that email
      
  		    $combinedResult = SetNewUserPassword($em);
      
      
  		} else {
  		    $status = "Error";
  		    $statusMsg = "Database says: " . $sql . "<br>" . $conn->error;
 		 }

  
  		$conn->close();
  
 		 }

		} else {
    		  $status = "Error";
    		  $statusMsg = "POST data not found.";

		}
	}
}


?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ProjectPro - Add New Users</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <!-- <link href="css/sb-admin.css" rel="stylesheet">  -->

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<?php

if($statusMsg==="email already exists") {
	echo "
	<script>
		document.location.href = 'emailexists.php?email=" . $em . "';
	</script>
	";

}

?>

<div id="page-wrapper">

	<div class="container-fluid">

		<!-- Page Heading -->
		<div class="row">

			<form role="form" name="newuser" id="newuser" method="post" action="admin-addnewuser.php">

			<div class="form-group">
				<label>First name:</label>
				<input class="form-control" name="firstname" id="firstname">
			</div>

			<div class="form-group">
				<label>Last name:</label>
				<input class="form-control" name="lastname" id="lastname">
			</div>

			<div class="form-group">
				<label>Email address:</label>
				<input class="form-control" type="email" name="emailaddr" id="emailaddr">
			</div>

			<div class="form-group">
				<label for="sel1">Employee Type:</label>
				<select class="form-control" id="usertypes" name="usertype">
					<option>Select Employee Type</option>
					<option>-----</option>
					<option>Field Employee</option>
					<option>Approver</option>
					<option>Accountant</option>
				</select>
			</div>

			<div class="form-group">
				<button type="button" class="btn btn-primary" onClick="GetSelectedItem()">Save and Continue</button>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
				<?php  
				// IF ANY MESSENGES ARE TO BE CONVEYED TO THE USER that happens right here
				
					if (isset($_POST['firstname']))  {
						echo "Last action: " .  $statusMsg;   
					}
					if (isset($_GET['msg']))  {
						echo "Last action: " .  $statusMsg . ". Source: " . urldecode($_GET['src']);
					}
				?>
			</div>
			
		</div> <!-- row -->
		
<!-- ---------------- JAVASCRIPT REGION ---------------- -->

<script>

function GetSelectedItem() {

     var option = document.getElementById('usertypes').value;
     var fname = document.getElementById('firstname').value;
     var lname = document.getElementById('lastname').value;
     var email = document.getElementById('emailaddr').value;
     
     var formOK = true;
     
     if (option==='Select Employee Type' || option==='-----') {
     	var formOK = false;
     	alert('You must select a User Type.');
     } 
     
     if (fname==='' || lname==='' || email==='') {
     	var formOK = false;
     	alert('No field may be left blank.');
     }
     
	if (formOK) {
		document.getElementById("newuser").submit();
	}
} 

</script>

<!-- ---------------- END JAVASCRIPT REGION ---------------- -->

	</div>
</div>

</body>