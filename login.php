<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Project Pro Login">
    <meta name="author" content="willcate">

    <title>ProjectPro | Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            	<!--    
            	<a class="navbar-brand" href="index.html">SB Admin</a>
            	-->
            	<a href="index.html"><img src="img/logo-web-banner.png" style="display: block; margin: 0 auto;"></a>
            </div>
            
            
            
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <!-- There is nothing in this drop-down menu, because nobody is logged in -->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <!-- There is nothing in this drop-down menu, because nobody is logged in -->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Logged Out <b class="caret"></b></a>
                    <ul class="dropdown-menu">
						<!-- There is nothing in this drop-down menu, because nobody is logged in -->
                    </ul>
                </li>
            </ul>
            
            
            
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="#"><i class="fa fa-fw fa-dashboard"></i> Menu One</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-bar-chart-o"></i> Menu Two</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-table"></i> Menu Three</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-edit"></i> Menu Four</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-desktop"></i> Menu Five</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-wrench"></i> Menu Six</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome!
                            <small>Please log in to your account:</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Login
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                
                <div class="row">
                    <div class="col-lg-3 text-center">
                        <!-- Nothing goes here -->
                    </div>
                    <div class="col-lg-6 text-left">
                        <div class="panel panel-default">
                            <div class="panel-body">
                            
                            <?php
                            // add error message if necessary
                            if (isset($_GET['msg'])) {
                            
                            	if ($_GET['msg']==="email") {
                            		echo "<div class='alert alert-danger'>
                            		Sorry, a user account with that email address was not found.
                            		Please try again, or contact your ProjectPro company administrator for assistance.
                            		</div>";
                            	}
                            	if ($_GET['msg']==="pswd") {
                            		echo "<div class='alert alert-danger'>
                            		Sorry, the password supplied for that address was not correct.
                            		Please try again, or <a href='requestpasswordreset.php'>click here to reset your password.</a>
                            		</div>";
                            	
                            	}
                            	if ($_GET['msg']==="logout") {
                            		echo "<div class='alert alert-info'>
                            		<b>You are logged out!</b>
                            		See you later.
                            		</div>";
                            	
                            	}
                            	if ($_GET['msg']==="pswdchgesuccess") {
                            		echo "<div class='alert alert-info'>
                            		<b>Password was successfully changed.</b>
                            		</div>";
                            	
                            	}
                            
                            }
                            
                            ?>
                            
                                <form role="form" name="login" id="login" method="post" action="userauth.php">
                                
									<div class="form-group">
										<label>Your email address:</label>
										
										<?php
										
										if (isset($_POST['emailaddr'])) {
											$em = $_POST['emailaddr'];
											echo "<input class='form-control' type='email' name='emailaddr' value='" . $em . "'>";
										} else { 
											if (isset($_GET['emailaddr'])) {
												$em = $_GET['emailaddr'];
												echo "<input class='form-control' type='email' name='emailaddr' value='" . $em . "'>";
											} else {
												echo "<input class='form-control' type='email' name='emailaddr'>";
											}
										}
											
                                		?>
                                		
									</div>  <!-- /.form-group -->
									<div class="form-group">
										<label>Your password:</label>
                                		<input class="form-control" type="password" name="pswd">
									</div>  <!-- /.form-group -->
									<div class="form-group">
										<button type="button" class="btn btn-default btn-primary" onClick="handleSubmit()">Login</button>
									</div>  <!-- /.form-group -->

								</form>
                            </div>  <!-- /.panel-body -->
                            
                        </div>
                        <h4>Or, <a href="initialsetup.php">click here to register a new company</a></h4>
                    </div>
                    <div class="col-lg-3 text-center">
                    	
                         <!-- Nothing goes here -->
                    </div>
                </div>
                <!-- /.row -->


                
                

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    
    <script>
		function handleSubmit() {
			var input0 = document.getElementsByTagName("input")[0].value;
			var input1 = document.getElementsByTagName("input")[1].value;
			if (input1.length < 8) {
				alert("Password must be at least 8 chars long.");
			} else {
				document.getElementById("login").submit();
			}
			
		}
	</script>	


</body>

</html>