<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ProjectPro - Add New Users</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <!-- <link href="css/sb-admin.css" rel="stylesheet">  -->

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<?php

if (isset($_POST['email']))  {

	$em = $_POST['email'];
	$sq = "'";
	 
	include 'include/db.conf.php';


	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	
	// Check connection
	if ($conn->connect_error) {
		// Database connection error
		$status = "Error";
		$statusMsg = "Connection failed: " . $conn->connect_error;
	} else { 
	
		$sql = "SELECT first_name,last_name FROM USERS WHERE email_addr = " . $sq . $em . $sq;
		if (!$result = $conn->query($sql)) {
			// SQL query error
			$status = "Error";
			$statusMsg = "Database says: " . $sql . "<br>" . $conn->error;
		} else {
	
			if ($result->num_rows === 0) {
				// email addr not found
				$status = "Error";
				$statusMsg = "The email address was not found.";
				
				// return user to login page with message
				
				header('Location: addnewuser.php?msg=unexpected&src=' . );
				
				echo "
	
	<script>
		document.location.href = 'admin-addnewuser.php?msg=unexpected&src='" . urlencode(__FILE__) . ";
	</script>

				";	
				
			} else {
	
				// if we've gotten this far, then a row was found. Password is either right or wrong.
	
				$resultArray = $result->fetch_assoc();
				$hash        = $resultArray['pw_hash'];
				$userType    = $resultArray['user_type'];
				$fName       = $resultArray['first_name'];
				$lName       = $resultArray['last_name'];
				$acctEmail   = $resultArray['acct_email'];


}

?>

<div id="page-wrapper">

	<div class="container-fluid">

		<!-- Page Heading -->
		<div class="row">

			<form role="form" name="newuser" id="newuser" method="post" action="admin-addnewuser.php">

			<div class="form-group">
				<label>That user already exixts:</label>
			</div>

			

			<div class="form-group">
				<button type="button" class="btn btn-primary" onClick="GetSelectedItem()">Save and Continue</button>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php  
					if (isset($_POST['firstname']))  {
						echo "Last action: " .  $statusMsg;   
					}
				?>
			</div>
			
		</div> <!-- row -->
		
<!-- ---------------- JAVASCRIPT REGION ---------------- -->

<script>



</script>

<!-- ---------------- END JAVASCRIPT REGION ---------------- -->

	</div>
</div>

</body>
