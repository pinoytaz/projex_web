<html><body>

<?php
	
$sq     = "'";
$comma  = ",";

include 'include/db.conf.php';

	
	// Create connection
	
$conn = new mysqli($servername, $username, $password, $dbname);
		
	// Check connection
	
if ($conn->connect_error) {
	
		// Database connection error, set JSON message and get out
		
	$status = "Error";
	$statusMsg = "Connection failed: " . $conn->connect_error;
	$jsondata = ['status' => $status, 'body' => $statusMsg];
	header('Content-type: application/json');
	exit(json_encode($jsondata));
}
	// otherwise, continue
	
$sql = "SELECT jpeg_data,purchase_data FROM PURCHASES WHERE email_addr = 'test2@test.com' and when_submitted = '2017-06-26 02:27:00'";
	
if (!$result = $conn->query($sql)) {
	
	// SQL query error, set JSON message and get out
	
	$status = "Error";
	$statusMsg = "Database says: " . $sql . "<br>" . $conn->error;
	$jsondata = ['status' => $status, 'body' => $statusMsg];
	header('Content-type: application/json');
	exit(json_encode($jsondata));
		
} 
	
if ($result->num_rows === 0) {

	// email addr not found, set JSON message and get out
	
	$status = "Error";
	$statusMsg = "The row containing the image was not found.";
	$jsondata = ['status' => $status, 'body' => $statusMsg, 'sql' => $sql];
	header('Content-type: application/json');
	exit(json_encode($jsondata));
}
	
	// if we've gotten this far, then a row was found. Retrieve the image data.
$row = mysqli_fetch_assoc($result);
$imgData = $row['jpeg_data'];
$pData = $row['purchase_data'];
//echo $imgData;

$imageBin = base64_encode(Hex2Bin($imgData));
	
?>

<img src="data:image/jpg;charset=utf8;base64,<?php echo $imageBin ?>"/>

<?php
echo "<br><br><xmp>";
echo $pData;
echo "</xmp>";

?>

</body></html>
	