<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Project Pro Login">
    <meta name="author" content="willcate">

    <title>ProjectPro | Start New Company</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    
    <!-- Handle Submit Button Click -->
    
	<script>
		function handleSubmit() {
			var vCompname = document.getElementById("compname").value;
			var vFirstname = document.getElementById("firstname").value;
			var vLastname = document.getElementById("lastname").value;
			var vEmail = document.getElementById("emailaddr").value;
			
			switch (true) {
				case (vCompname == ""):
					alert("The field Company Name cannot be left blank.");
					break
				case (vFirstname == ""):
					alert("The field First Name cannot be left blank.");
					break
				case (vLastname == ""):
					alert("The field Last Name cannot be left blank.");
					break
				case (vEmail == ""):
					alert("The field Email Address cannot be left blank.");
					break
				default:
					vForm = document.getElementById("CompanyBegin");
					vForm.submit();
			}
						
		}
	</script>	


</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            	<!--    
            	<a class="navbar-brand" href="index.html">SB Admin</a>
            	-->
            	<a href="index.html"><img src="img/logo-web-banner.png" style="display: block; margin: 0 auto;"></a>
            </div>
            
            
            
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <!-- There is nothing in this drop-down menu, because nobody is logged in -->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <!-- There is nothing in this drop-down menu, because nobody is logged in -->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Logged Out <b class="caret"></b></a>
                    <ul class="dropdown-menu">
						<!-- There is nothing in this drop-down menu, because nobody is logged in -->
                    </ul>
                </li>
            </ul>
            
            
            
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <!-- Sidebar Menu Items would have gone here -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Start your company account...
                            <small>It's free!</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Initial Setup
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                
                <div class="row">
                    <div class="col-lg-3 text-center">
                        <!-- Nothing goes here -->
                    </div>
                    <div class="col-lg-6 text-left">
                        <div class="panel panel-default">
                            <div class="panel-body">
                            
                            <?php
                            // add error message if necessary
                            if (isset($_GET['msg'])) {
                            
                            	if ($_GET['msg']==="email") {
                            		echo "<div class='alert alert-danger'>
                            		Sorry, a user account with that email address already exists.
                            		Please try again, or contact ProjectPro Support for assistance.
                            		</div>";
                            	}
                            
                            }
                            
                            ?>
                            
                                <form role="form" name="CompanyBegin" id="CompanyBegin" method="post" action="initialsetup2.php">
                                
									<div class="form-group">
										<label>Your company name:</label>
										<input class="form-control" name="compname" id="compname">
									</div>  <!-- form-group -->
									
									<div class="form-group">
										<label>First name:</label>
										<input class="form-control" name="firstname" id="firstname">
									</div>  <!-- form-group -->

									<div class="form-group">
										<label>Last name:</label>
										<input class="form-control" name="lastname" id="lastname">
									</div>  <!-- form-group -->

									<div class="form-group">
										<label>Your email address:</label>
										
										<?php
										
										if (isset($_POST['emailaddr'])) {
											$em = $_POST['emailaddr'];
											echo "<input class='form-control' type='email' id='emailaddr' name='emailaddr' value='" . $em . "'>";
										} else { 
											echo "<input class='form-control' type='email' id='emailaddr' name='emailaddr'>";
										}
											
                                		?>
                                		
									</div>  <!-- /.form-group -->
									
									<div class="form-group">
										<label>You are the Administrator. A company may have only one administrator. If you are also the Accountant, check the box below.</label>
										<div class="checkbox">
  											<label><input type="checkbox" value="1" id="isalsoacct" name="isalsoacct">I am also the Accountant</label>
										</div>
									</div>  <!-- /.form-group -->
									
                                  <div class="form-group">
										<label for="accountant_email">Accountant's Email:</label>
										<input class="form-control" name="accountant_email" id="accountant_email">
									</div>  <!-- /.form-group -->
                                  
									<div class="form-group">
										<button type="button" class="btn btn-default btn-primary" onClick="handleSubmit()">Continue</button>
									</div>  <!-- /.form-group -->

								</form>
                            </div>  <!-- /.panel-body -->
                            
                        </div>
                    </div>
                    <div class="col-lg-3 text-center">
                    	
                         <!-- Nothing goes here -->
                    </div>
                </div>
                <!-- /.row -->


                
                

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    


</body>

</html>