<?php

// This script performs the password change, upon user's submission of new password via form in "resetpassword.php"

if (isset($_POST['emailaddr'])) 
{  

	$em = $_POST['emailaddr'];
	$pw = password_hash($_POST['password1'], PASSWORD_DEFAULT);
	$sq = "'";
	 
	include 'include/db.conf.php';


	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	
	// Check connection
	if ($conn->connect_error) {
		// Database connection error
		$status = "Error";
		$statusMsg = "Connection failed: " . $conn->connect_error;
	} else { 
	
		$sql = "UPDATE USERS SET pw_hash = '" . $pw . "' WHERE email_addr = " . $sq . $em . $sq;
	
		if (!$result = $conn->query($sql)) {
			// SQL query error
			$status = "Error";
			$statusMsg = "Database says: " . $sql . "<br>" . $conn->error;
		} else {
	
			if ($result->num_rows === 0) {
				// email addr not found
				$status = "Error";
				$statusMsg = "The email address was not found.";
				
				// return user to login page with message
				
				header('Location: login.php?msg=email');
				
			} else {
	
				// if we've gotten this far, then the row was updated.
				
				$status = "Success";
				$statusMsg = "The password was changed.";
	

	  		}
		}
	}
	
	$conn->close();
  
} else {
      $status = "Error";
      $statusMsg = "POST data not found.";
}

// OK, commence with the page building.

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ProjectPro | Resetting Password</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            	
            	<a href="index.php"><img src="img/logo-web-banner.png" style="display: block; margin: 0 auto;"></a>
            </div>
            
            
            
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <!-- There is nothing in this drop-down menu, because nobody is logged in -->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <!-- There is nothing in this drop-down menu, because nobody is logged in -->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Logged Out <b class="caret"></b></a>
                    <ul class="dropdown-menu">
						<!-- There is nothing in this drop-down menu, because nobody is logged in -->
                    </ul>
                </li>
            </ul>



            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            
            <?php
            // set the content of the sidebar menu items
            
            $menuItem1 = "<a href='#'><i class='fa fa-fw fa-dashboard'></i> Menu One</a>";
            $menuItem2 = "<a href='#'><i class='fa fa-fw fa-bar-chart-o'></i> Menu Two</a>";
            $menuItem3 = "<a href='#'><i class='fa fa-fw fa-table'></i> Menu Three</a>";
            $menuItem4 = "<a href='#'><i class='fa fa-fw fa-edit'></i> Menu Four</a>";
            $menuItem5 = "<a href='#'><i class='fa fa-fw fa-desktop'></i> Menu Five</a>";
            $menuItem6 = "<a href='#'><i class='fa fa-fw fa-wrench'></i> Menu Six</a>";
            
            ?>
            
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <?php echo $menuItem1; ?>
                    </li>
                    <li>
                        <?php echo $menuItem2; ?>
                    </li>
                    <li>
                        <?php echo $menuItem3; ?>
                    </li>
                    <li>
                        <?php echo $menuItem4; ?>
                    </li>
                    <li>
                        <?php echo $menuItem5; ?>
                    </li>
                    <li>
                        <?php echo $menuItem6; ?>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Password Change
                            <small>Result</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Database error
                            </li>
                        </ol>
                        
                        
                    </div>
                </div>
                <!-- /.row -->
                
                <div class="row">
                    <div class="col-lg-3 text-center">
                        <!-- Nothing goes here -->
                    </div>
                    <div class="col-lg-6 text-left">
                        <div class="panel panel-default">
                            <div class="panel-body">
                            
                            
							<?php
                            
                            if ($status === "Success") {

	                            echo "<h4>Your password was successfully changed!</h4>";
	                            echo "<form role='form' name='login' id='login' method='post' action='login.php'>";
	                            echo "<input type='hidden' name='emailaddr' value='" . $em . "'>";
	                            echo "<input type='submit' class='btn btn-primary' value='Return To Login'>";
	                            
	                        } else {
                            
								echo "<h4>Sorry, an unexpected error occured. If this error persists, please contact ProjectPro support with this error message:</h4>";
								echo "<hr><p>" . $statusMsg . "</p>";
                            }
                            
                            ?>
                            
                            </div>  <!-- /.panel-body -->
                        </div>
                    </div>
                    <div class="col-lg-3 text-center">
                         <!-- Nothing goes here -->
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
