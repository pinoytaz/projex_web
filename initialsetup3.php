<?php

include 'include/license_key.class.php';
include 'include/db.conf.php';
$key = new license_key();
$key->keylen = 16;
$key->formatstr = "4444";
$key->software = "pp";
$newkey = $key->codeGenerate("user");
							
if (isset($_POST['emailaddr'])) 
{  

	$cn = $_POST['compname'];
	$fn = $_POST['firstname'];
	$ln = $_POST['lastname'];
	$em = $_POST['emailaddr'];
	$aa = $_POST['isalsoacct'];
    $accountantEmail = $_POST['accountant_email'];
	$pw = password_hash($_POST['password1'], PASSWORD_DEFAULT);
	
	// if "also the accountant" was checked, then the user's acct_email foeld gets the same value as user_email
	if ($aa === "1") {
		$ae = $em;
	} else {
		$ae = "unset";
	}
	
	$et = "administrator";
    if(isset($accountantEmail) || $accountantEmail!=""){
        $et = "field-emp";
        $ae = $accountantEmail;
    }
	$sq = "'";
	$fb = "', '";
	
	
	$newUserValues = $sq . $em . $fb . $pw . $fb . $fn . $fb . $ln . $fb . $et . $fb . $ae . $sq;
	
	$newCompValues = $sq . $cn . $fb . $ln . $fb . $em . $sq;
	
	    // Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	
	// Check connection
	if ($conn->connect_error) {
		$status = "Error";
		$statusMsg = "Connection failed: " . $conn->connect_error;
	} else {
	
		// Create those new rows
		
		$sql = "INSERT INTO USERS (email_addr, pw_hash, first_name, last_name, user_type, acct_email)
					VALUES (" . $newUserValues . ")";

		if ($conn->query($sql) === TRUE) {
			$status = "Success";
			$statusMsg = "New user record created successfully";
		} else {
			$status = "Error";
			$statusMsg = "New Record Insert failed";
		}
		
      
                header("Location: login.php");
                die();
      
		$sql = "INSERT INTO COMPANIES (comp_name, pp_license, acct_email)
					VALUES (" . $newCompValues . ")";

		if ($conn->query($sql) === TRUE) {
			$status = "Success";
			$statusMsg = "New company record created successfully";
		} else {
			$status = "Error";
			$statusMsg = "New Record Insert failed";
		}


	}

	
    

} else {
      $status = " - Error";
      $statusMsg = " - POST data not found.";
}



?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Project Pro Login">
    <meta name="author" content="willcate">

    <title>ProjectPro | Start New Company</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</head>

<body>
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            	<!--    
            	<a class="navbar-brand" href="index.html">SB Admin</a>
            	-->
            	<a href="index.html"><img src="img/logo-web-banner.png" style="display: block; margin: 0 auto;"></a>
            </div>
            
            
            
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <!-- There is nothing in this drop-down menu, because nobody is logged in -->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <!-- There is nothing in this drop-down menu, because nobody is logged in -->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Logged Out <b class="caret"></b></a>
                    <ul class="dropdown-menu">
						<!-- There is nothing in this drop-down menu, because nobody is logged in -->
                    </ul>
                </li>
            </ul>
            
            
            
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <!-- Sidebar Menu Items would have gone here -->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Start your company account...
                            <small>It's free!</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Initial Setup
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
                
                <div class="row">
                    <div class="col-lg-3 text-center">
                        <!-- Nothing goes here -->
                    </div>
                    <div class="col-lg-6 text-left">
                        <div class="panel panel-default">
                            <div class="panel-body">
                            
                            <?php
                            
                            include 'include/license_key.class.php';
                            $key = new license_key();
							$key->keylen = 16;
							$key->formatstr = "4444";
							$key->software = "pp";
							$newkey = $key->codeGenerate("user");
							
                            
                            ?>
                            
                                <form role="form" name="setup" id="setup" method="post" action="index.php">
                                
									<div class='form-group'>
										<label>Your ProjectPro license has been automatically generated:</label>
										<?php
										echo "<input class='form-control' name='license' id='license' readonly value='" . $newkey . "'>";
										?>
									</div>  <!-- /.form-group -->
	
	
		<input type='submit' class='btn btn-primary btn-load btn-default' data-loading-text='Saving...' value='Save and Continue'>

								</form>
                            </div>  <!-- /.panel-body -->
                            
                        </div>
                    </div>
                    <div class="col-lg-3 text-center">
                    	
                         <!-- Nothing goes here -->
                    </div>
                </div>
                <!-- /.row -->


                
                

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    
	
	    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    

	
	 <!-- Password Validation Javascript -->
    
    <script type="text/javascript">
	$("input[type=password]").keyup(function(){
    var ucase = new RegExp("[A-Z]+");
	var lcase = new RegExp("[a-z]+");
	var num = new RegExp("[0-9]+");
	
	if($("#password1").val().length >= 8){
		$("#8char").removeClass("glyphicon-remove");
		$("#8char").addClass("glyphicon-ok");
		$("#8char").css("color","#00A41E");
	}else{
		$("#8char").removeClass("glyphicon-ok");
		$("#8char").addClass("glyphicon-remove");
		$("#8char").css("color","#FF0004");
	}
	
	if(ucase.test($("#password1").val())){
		$("#ucase").removeClass("glyphicon-remove");
		$("#ucase").addClass("glyphicon-ok");
		$("#ucase").css("color","#00A41E");
	}else{
		$("#ucase").removeClass("glyphicon-ok");
		$("#ucase").addClass("glyphicon-remove");
		$("#ucase").css("color","#FF0004");
	}
	
	if(lcase.test($("#password1").val())){
		$("#lcase").removeClass("glyphicon-remove");
		$("#lcase").addClass("glyphicon-ok");
		$("#lcase").css("color","#00A41E");
	}else{
		$("#lcase").removeClass("glyphicon-ok");
		$("#lcase").addClass("glyphicon-remove");
		$("#lcase").css("color","#FF0004");
	}
	
	if(num.test($("#password1").val())){
		$("#num").removeClass("glyphicon-remove");
		$("#num").addClass("glyphicon-ok");
		$("#num").css("color","#00A41E");
	}else{
		$("#num").removeClass("glyphicon-ok");
		$("#num").addClass("glyphicon-remove");
		$("#num").css("color","#FF0004");
	}
	
	if($("#password1").val() == $("#password2").val()){
		$("#pwmatch").removeClass("glyphicon-remove");
		$("#pwmatch").addClass("glyphicon-ok");
		$("#pwmatch").css("color","#00A41E");
	}else{
		$("#pwmatch").removeClass("glyphicon-ok");
		$("#pwmatch").addClass("glyphicon-remove");
		$("#pwmatch").css("color","#FF0004");
	}
});
    </script>




</body>

</html>