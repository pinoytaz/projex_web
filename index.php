<?php
$status="";
$cookie_name = "ProjectProLogin";
if(!isset($_COOKIE[$cookie_name])) {
	// no login cookie; go to login page
	header('Location: login.php'); 
} else {
	// read the contents of the cookie!
	$FieldsArray = explode('&&&&', $_COOKIE[$cookie_name]);
	$em = $FieldsArray[0];
	$token = $FieldsArray[1];
	
	// if $token was passed as a POST value, use that value instead (user might have just changed password).
	
	if (isset($_POST['token'])) {
		$token = $_POST['token'];
	}
	
	// begin debug code
	
//	echo $_COOKIE[$cookie_name] . "<br><br>";
//	echo $em . "<br><br>";
//	echo $token . "<br><br>";
	
	// begin debug code
	
		$sq = "'";
	 
		include 'include/db.conf.php';

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	
	// Check connection
	if ($conn->connect_error) {
		// Database connection error
		$status = "Error";
		$statusMsg = "Connection failed: " . $conn->connect_error;
	} else { 
	
		$sql = "SELECT last_name,first_name,user_type,email_addr,acct_email FROM USERS WHERE pw_hash = " . $sq . $token . $sq;

		if (!$result = $conn->query($sql)) {
			// SQL query error
			$status = "Error";
			$statusMsg = "Database says: " . $sql . "<br>" . $conn->error;
		} else {
	
			if ($result->num_rows === 0) {
				// email addr not found
				$status = "Error";
				$statusMsg = "The account ID passed to this page was not found.";
				
				// return user to login page with message
					
				header('Location: login.php?msg=email');
				
			} else {
	
				// if we've gotten this far, then a row was found. 
	
				$resultArray = $result->fetch_assoc();
				$usertype    = ($resultArray['user_type']=='field-emp'?'employee':$resultArray['user_type']);
//              $usertype    = $resultArray['user_type'];
				$fName       = $resultArray['first_name'];
				$lName       = $resultArray['last_name'];
				$emailAddr   = $resultArray['email_addr'];
				$acctEmail   = $resultArray['acct_email'];
				$status      = "Success";
				$statusMsg   = "This page view is authorized.";
				$fullname    = $fName . " " . $lName;
				
                
			}
		}
	}
}	

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ProjectPro - Home</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            	<!--    
            	<a class="navbar-brand" href="index.html">SB Admin</a>
            	-->
            	<a href="index.html"><img src="img/logo-web-banner.png" style="display: block; margin: 0 auto;"></a>
            </div>
            
            
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <!-- Things should go here -->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <!-- Things should go here -->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $fullname; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
						<!-- options which should appear in this menu: Logout, Account Settings -->
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            
            
            
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            
            <?php
            // set the content of the sidebar menu items
            
            $menuItem1 = "<a href='#'><i class='fa fa-fw fa-dashboard'></i> Menu One</a>";
            $menuItem2 = "<a href='#'><i class='fa fa-fw fa-bar-chart-o'></i> Menu Two</a>";
            $menuItem3 = "<a href='#'><i class='fa fa-fw fa-table'></i> Menu Three</a>";
            $menuItem4 = "<a href='#'><i class='fa fa-fw fa-edit'></i> Menu Four</a>";
            $menuItem5 = "<a href='#'><i class='fa fa-fw fa-desktop'></i> Menu Five</a>";
            $menuItem6 = "<a href='#'><i class='fa fa-fw fa-wrench'></i> Menu Six</a>";
            
            ?>
            
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <?php echo $menuItem1; ?>
                    </li>
                    <li>
                        <?php echo $menuItem2; ?>
                    </li>
                    <li>
                        <?php echo $menuItem3; ?>
                    </li>
                    <li>
                        <?php echo $menuItem4; ?>
                    </li>
                    <li>
                        <?php echo $menuItem5; ?>
                    </li>
                    <li>
                        <?php echo $menuItem6; ?>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                        
                        <?php
						$printusertype = ucwords($usertype);
						echo $printusertype;
                        ?>
 
                        <small>Dashboard</small>
                            
                            
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Dashboard</a>
                            </li>
                            <!-- LOWER BREADCRUMB LEVELS GO HERE  -->
                        </ol>
                        
                        <p>
                        <?php
                        
                        if($status === "Success") {
                        	echo "This section for testing purposes only:<br>";
                        	echo "<b>The status: </b>" . $statusMsg . "<br>";
                        	echo "<b>My first name: </b>" . $fName . "<br>";
                        	echo "<b>My last name: </b>" . $lName . "<br>";
                        	echo "<b>My email address: </b>" . $emailAddr . "<br>";
                        	echo "<b>My user type: </b>" . $usertype . "<br><br>";
                        	
                        }
                        
                        // switch for value of usertype
						switch ($usertype) {
							case "approver":
								
								// display approver stuff here
								
								echo "<p>Approver-specific stuff will appear here.</p>";
								break;
								
							case "accountant":
								
								// display accountant stuff here
								
								echo "<p>Accountant-specific stuff will appear here.</p>";
								break;
								
							case "administrator":
								
								// display administrator stuff here
								
								$sql = "SELECT last_name,first_name,user_type,email_addr FROM USERS WHERE email_addr != " . $sq . $emailAddr . $sq . " AND acct_email = " . $sq . $acctEmail . $sq;
								if (!$result = $conn->query($sql)) {
									// SQL query error
									$status = "Error";
									$statusMsg = "Database says: " . $sql . "<br>" . $conn->error;
								} else {
									$numberofrowsfound = $result->num_rows;
									if ($numberofrowsfound === 0) {
										// no connected users found
										echo "no records found";
									} else {
										echo $numberofrowsfound . " rows found:<br><br>";
										
										// draw the table of users
										
										echo "<table class='table table-striped'>\n";
										echo "<tr>\n";
										echo "<th>First Name</th>\n";
										echo "<th>Last Name</th>\n";
										echo "<th>EMail Addr</th>\n";
										echo "<th> </th>\n";
										echo "</tr>\n";

										while($row = $result->fetch_assoc()) {
											echo "<tr>\n";
											echo "<td>" . $row["first_name"] . "</td>\n";
											echo "<td>" . $row["last_name"] . "</td>\n";
											echo "<td>" . $row["email_addr"] . "</td>\n";
											echo "<td>" . " <a href='admin-sendpasswordchangeemail.php?emailaddr=" . $row["email_addr"] . "'>Password Change</a></td>\n";
  											echo "</tr>\n";
  										}
  										

										echo "</table>";
									
										// any other widgets go here
										
										echo "<br><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#myModal2'>Add New Users</button>";
										echo "&nbsp;&nbsp;<button type='button' class='btn btn-primary'>Manage Projects</button>";
										echo "&nbsp;&nbsp;<button type='button' class='btn btn-primary'>Manage Payment Types</button>";
									}
								}
								break;
						}

                        
                        
                        
                        
                        
                        ?>
                        </p>
                        



<!-- Modal 2 -->
<div id="myModal2" class="modal fade" role="dialog">
	<div class="modal-dialog">

        <!-- Modal content -->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add New User</h4>
			</div>
		<div class="modal-body">
      
		<iFrame src="admin-addnewuser.php" width="578" height="348" style="border:none;"></iFrame> 
 
	</div>
	
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Done Adding New Users</button>
	</div>
</div>

<!-- END Modal -->


                    
                        
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
